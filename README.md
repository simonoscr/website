<div align="center">

# simonoscr.gitlab.io

[![built with nix-flake](https://img.shields.io/static/v1?logo=nixos&logoColor=white&label=&message=Built%20with%20Nix%20Flakes&color=41439a)](https://builtwithnix.org)


[![pipeline status](https://gitlab.com/simonoscr/simonoscr.gitlab.io/badges/main/pipeline.svg)](https://gitlab.com/simonoscr/simonoscr.gitlab.io/-/commits/main)

</div>
